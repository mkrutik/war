FROM debian:8


RUN apt-get update; \
    apt-get install -y \
    nasm \
    gcc \
    make \
    binutils \
    file \
    strace \
    vim

# docker build -t war .
# docker run --cap-add=SYS_PTRACE --security-opt seccomp=unconfined
