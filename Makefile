# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/05/22 21:35:20 by mkrutik           #+#    #+#              #
#    Updated: 2019/10/05 15:26:12 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = War

CC = gcc
CFLAGS = -Wall -Wextra -fPIC -m64 \
			-fno-stack-protector -fno-asynchronous-unwind-tables -std=gnu99
			# -g

ASM = nasm
AFLAGS = -f elf64

LD = ld
LDFLAGS = --gc-sections --print-gc-sections -pie --dynamic-linker /lib64/ld-linux-x86-64.so.2

INC = includes

SDIR = src

SRCS =	asm/loader.asm \
		asm/ft_open.asm \
		asm/ft_close.asm \
		asm/ft_lseek.asm \
		asm/ft_mmap.asm \
		asm/ft_munmap.asm \
		asm/ft_time.asm \
		asm/ft_read.asm \
		asm/syscalls.asm \
		elf.c \
		file_helpers.c \
		libft_helpers.c \
        prepare_file_for_injection_64.c \
		encryption.c \
		elf_64.c \
		elf_64_parse.c \
		xor_encrypt.c \
		md5.c
		# asm/ft_write.asm




SRCS_R = $(SRCS:%=$(SDIR)/%)

HEADERS1 = war.h

HEADERS_R = $(HEADERS:%.h=$(INC)/%.h)

ODIR = Objects

OBJ_T = $(SRCS:%.c=%.o)

OBJ = $(OBJ_T:%.asm=%.o)

OBJ_R = $(OBJ:%.o=$(ODIR)/%.o)


NAME_2 = patcher_for_patcher

SRCS_2 = \
		patcher_4_patcher/main.c \
        xor_encrypt.c \
		file_helpers.c \
		libft_helpers.c \
		elf_64_parse.c \
		asm/ft_open.asm \
		asm/ft_close.asm \
		asm/ft_write.asm \
		asm/ft_read.asm \
		asm/ft_lseek.asm \
		asm/ft_mmap.asm \
		asm/syscalls.asm \
		asm/ft_munmap.asm \
		md5.c \
		encryption.c


SRCS_R_2 = $(SRCS_2:%=$(SDIR)/%)
OBJ_T_2 = $(SRCS_2:%.c=%.o)
OBJ_2 = $(OBJ_T_2:%.asm=%.o)
OBJ_R_2 = $(OBJ_2:%.o=$(ODIR)/%.o)

all : $(NAME)

$(NAME)_build : $(ODIR) $(OBJ_R) $(NAME_2)
	$(LD) $(LDFLAGS) -o $(NAME) $(OBJ_R)

$(NAME) : $(NAME)_build
	./$(NAME_2) $(NAME)
	objcopy --only-keep-debug $(NAME) $(NAME).dSym
	strip --strip-all $(NAME)
	objcopy --add-gnu-debuglink=$(NAME).dSym $(NAME)

$(NAME_2) : $(ODIR) $(OBJ_R_2)
	$(CC) -o $(NAME_2) $(OBJ_R_2)

.PHONY: clean fclean re

clean :
	rm -f $(OBJ_R)
	rm -rf $(ODIR)

fclean : clean
	rm -f $(NAME) $(NAME).dSym
	rm -rf $(ODIR)

re : fclean $(NAME)

norm:
	make norm -C libft
	norminette $(SRCS_R) $(HEADERS_R)

add:
	make add -C libft
	git add $(SRCS_R) $(HEADERS_R)\
			.gitignore Makefile CMakeLists.txt author

$(ODIR)/%.o : $(SDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(ODIR)/%.o : $(SDIR)/%.asm
	$(ASM) $(AFLAGS) -o $@ $<

$(ODIR):
	mkdir -p $@
	mkdir -p $@/asm
	mkdir -p $@/patcher_4_patcher

pr_test: re
	cp /bin/ls /tmp/ls
	./$(NAME)
	mv /tmp/ls ./ls_patched
	cp /bin/ls /tmp/ls

setup2: re
	mkdir -p /tmp/test
	mkdir -p /tmp/test2
	cp /bin/ls /tmp/test
	cp /usr/bin/strings /tmp/test2

pr_test2: setup2
	./$(NAME)
	mv /tmp/test/ls ./ls_patched
	mv /tmp/test2/strings ./strings_patched
	cp /bin/ls /tmp/test
	cp /usr/bin/strings /tmp/test2


 #ln -s /lib64/ld-linux-x86-64.so.2 /lib/ld64.so.1
