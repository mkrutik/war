#include "war.h"

// RAX, RCX, RDX, RBX, RSI, RDI, R8-R15
// RSP, RBP - not used
#define RAX       0
#define RCX       1
#define RDX       2
#define RBX       3
#define RSI       4
#define RDI       5
#define R8        6
#define R9        7
#define R10       8
#define R11       9
#define R12       10
#define R13       11
#define R14       12
#define R15       13
#define LAST_REG  14

// Encryption operations
#define XOR        0
#define ADD        1
#define SUB        2
#define INC        3
#define DEC        4
#define NOT        5
#define LAST_INSTR 6

#define ROL        6
#define ROR        7

#define MAX_INSTRUCTION 20
#define MAX_RANDOM_SIZE 100

typedef struct      s_inst_data
{
    const uint8_t   pop_size;
    const uint8_t   pop[2]; // max size = 2
    const uint8_t   cmp[4]; // always size = 4
    const uint8_t   inc[3]; // always size = 3
    const uint8_t   dec[3]; // always size = 3
    // encrypt operation
    const uint8_t   inst_size;
    const uint8_t   rolb[4]; // max size = 4
    const uint8_t   rorb[4]; // max size = 4
    const uint8_t   xorb[4]; // max size = 4
    const uint8_t   addb[4]; // max size = 4
    const uint8_t   subb[4]; // max size = 4
    const uint8_t   incb[4]; // max size = 4
    const uint8_t   decb[4]; // max size = 4
    const uint8_t   notb[4]; // max size = 4
}                   t_inst_data;

typedef struct  s_enc_set
{
    uint8_t     instr[MAX_INSTRUCTION];
    uint8_t     data[MAX_INSTRUCTION];
}               t_enc_set;

typedef struct  s_enc_inf
{
    uint8_t     reg_data;
    uint8_t     reg_counter;

    t_enc_set   enc;
    t_enc_set   dec;
}               t_enc_inf;


static void generate_reg(uint8_t *reg_data, uint8_t *reg_counter, const uint8_t random[MAX_RANDOM_SIZE])
{
    *reg_data = random[0] % LAST_REG;

    uint8_t i = 1;
    do
    {
        *reg_counter = random[i] % LAST_REG;
        i++;
    } while (*reg_data != *reg_counter && i < MAX_RANDOM_SIZE);

    // If something went wrong
    if (*reg_data == *reg_counter)
        *reg_counter = ((*reg_data + 1) != LAST_REG) ? (*reg_data + 1) : RAX;
}

static void    generate_instructions(t_enc_inf *inf, const uint8_t random[MAX_RANDOM_SIZE])
{
    // Generate encryption instruction and data for them
    for (uint8_t inst = 0, r_byte = 0; inst < MAX_INSTRUCTION && r_byte < MAX_RANDOM_SIZE; ++inst, ++r_byte)
    {
        // Get instruction code in range 0 - LAST
        inf->enc.instr[inst] = random[r_byte] %  LAST_INSTR;
        ++r_byte; // Use one random byte for instruction code and next byte for data
        inf->enc.data[inst] = random[r_byte];
    }

#if 1
    // TODO: If something modified in encryption/decryption all cases should be tested !!!
    inf->enc.instr[0] = ROL;
    inf->enc.data[0] = 147;
    inf->enc.instr[1] = XOR;
    inf->enc.data[1] = 153;
    inf->enc.instr[2] = INC;
    inf->enc.instr[3] = SUB;
    inf->enc.data[3] = 22;
    inf->enc.instr[4] = DEC;
    inf->enc.instr[5] = NOT;
    inf->enc.instr[6] = ADD;
    inf->enc.data[6] = 50;
    inf->enc.instr[7] = ROR;
    inf->enc.data[7] = 227;
#endif

    // Generate decryption instruction
    int j_enc = MAX_INSTRUCTION - 1;
    for (uint8_t i_dec = 0; i_dec < MAX_INSTRUCTION && j_enc >= 0; ++i_dec, --j_enc)
    {
        if (XOR == inf->enc.instr[j_enc])
        {
            inf->dec.instr[i_dec] = XOR;
            inf->dec.data[i_dec] = inf->enc.data[j_enc];
        }
        else if (ADD == inf->enc.instr[j_enc])
        {
            inf->dec.instr[i_dec] = SUB;
            inf->dec.data[i_dec] = inf->enc.data[j_enc];
        }
        else if (SUB == inf->enc.instr[j_enc])
        {
            inf->dec.instr[i_dec] = ADD;
            inf->dec.data[i_dec] = inf->enc.data[j_enc];
        }
        else if (INC == inf->enc.instr[j_enc])
        {
            inf->dec.instr[i_dec] = DEC;
        }
        else if (DEC == inf->enc.instr[j_enc])
        {
            inf->dec.instr[i_dec] = INC;
        }
        else if (NOT == inf->enc.instr[j_enc])
        {
            inf->dec.instr[i_dec] = NOT;
        }
        else if (ROL == inf->enc.instr[j_enc])
        {
            inf->dec.instr[i_dec] = ROR;
            inf->dec.data[i_dec] = inf->enc.data[j_enc];
        }
        else if (ROR == inf->enc.instr[j_enc])
        {
            inf->dec.instr[i_dec] = ROL;
            inf->dec.data[i_dec] = inf->enc.data[j_enc];
        }
    }
}

static void    init(t_enc_inf *all_inf)
{
    uint8_t random[MAX_RANDOM_SIZE];

    // ft_memset(random, 0, 100);
    get_random_bytes(random);
    generate_reg(&(all_inf->reg_data), &(all_inf->reg_counter), random);
    generate_instructions(all_inf, random);

    // TODO: If something modified in encryption/decryption all cases should be tested !!!
    //        counter        data
    // RAX      +              +
    // RCX,     +              +
    // RDX,     +              +
    // RBX,     +              +
    // RSI,     +              +
    // RDI,     +              +
    // R8,      +              +
    // R9,      +              +
    // R10,     +              +
    // R11,     +              +
    // R12,     +              +
    // R13,     +              +
    // R14,     +              +
    // R15,     +              +
#if 0
    all_inf->reg_counter = R15;
    all_inf->reg_data = R14;
#endif
}

static void    encrypt_data(uint8_t *data, const size_t length, const t_enc_set *enc)
{
    for (size_t byte = 0; byte < length; ++byte)
    {
        for (uint8_t inst = 0; inst < MAX_INSTRUCTION; ++inst)
        {
            if (XOR == enc->instr[inst])
            {
                data[byte] ^= enc->data[inst];
            }
            else if (ADD == enc->instr[inst])
            {
                data[byte] += enc->data[inst];
            }
            else if (SUB == enc->instr[inst])
            {
                data[byte] -= enc->data[inst];
            }
            else if (INC == enc->instr[inst])
            {
                data[byte] += 1;
            }
            else if (DEC == enc->instr[inst])
            {
                data[byte] -= 1;
            }
            else if (NOT == enc->instr[inst])
            {
                data[byte] = ~(data[byte]);
            }
            else if (ROL == enc->instr[inst])
            {
                const uint8_t shift = enc->data[inst] % 8;
                data[byte] = (data[byte] << shift) | (data[byte] >> (8 - shift));
            }
            else if (ROR == enc->instr[inst])
            {
                const uint8_t shift = enc->data[inst] % 8;
                data[byte] = (data[byte] >> shift) | (data[byte] << (8 - shift));
            }
        }
    }
}


static void decryption(t_enc_inf *inf, uint8_t *pop, uint8_t *cmp, uint8_t *body)
{
    const t_inst_data inst_codes[LAST_REG] = {
        {
            // RAX
            .pop_size = 1,
            .pop = {0x58},                   // 58                   	pop    %rax
            .cmp = {0x48, 0x83, 0xf8, 0x00}, // 48 83 f8 00          	cmp    $0x0,%rax
            .inc = {0x48, 0xff, 0xc0},       // 48 ff c0             	inc    %rax
            .dec = {0x48, 0xff, 0xc8},       // 48 ff c8             	dec    %rax
            .inst_size = 2,
            .rolb = {0xc0, 0x00},            // c0 00 04             	rolb   $0x4,(%rax)
            .rorb = {0xc0, 0x08},            // c0 08 04             	rorb   $0x4,(%rax)
            .xorb = {0x80, 0x30},            // 80 30 02             	xorb   $0x2,(%rax)
            .addb = {0x80, 0x00},            // 80 00 02             	addb   $0x2,(%rax)
            .subb = {0x80, 0x28},            // 80 28 02             	subb   $0x2,(%rax)
            .incb = {0xfe, 0x00},            // fe 00                	incb   (%rax)
            .decb = {0xfe, 0x08},            // fe 08                	decb   (%rax)
            .notb = {0xf6, 0x10},            // f6 10                	notb   (%rax)
        },
        {
            // RCX
            .pop_size = 1,
            .pop = {0x59},                   // 59                   	pop    %rcx
            .cmp = {0x48, 0x83, 0xf9, 0x00}, // 48 83 f9 00          	cmp    $0x0,%rcx
            .inc = {0x48, 0xff, 0xc1},       // 48 ff c1             	inc    %rcx
            .dec = {0x48, 0xff, 0xc9},       // 48 ff c9             	dec    %rcx
            .inst_size = 2,
            .rolb = {0xc0, 0x01},            // c0 01 04             	rolb   $0x4,(%rcx)
            .rorb = {0xc0, 0x09},            // c0 09 04             	rorb   $0x4,(%rcx)
            .xorb = {0x80, 0x31},            // 80 31 02             	xorb   $0x2,(%rcx)
            .addb = {0x80, 0x01},            // 80 01 02             	addb   $0x2,(%rcx)
            .subb = {0x80, 0x29},            // 80 29 02             	subb   $0x2,(%rcx)
            .incb = {0xfe, 0x01},            // fe 01                	incb   (%rcx)
            .decb = {0xfe, 0x09},            // fe 09                	decb   (%rcx)
            .notb = {0xf6, 0x11},            // f6 11                	notb   (%rcx)
        },
        {
            // RDX
            .pop_size = 1,
            .pop = {0x5a},                   // 5a                   	pop    %rdx
            .cmp = {0x48, 0x83, 0xfa, 0x00}, // 48 83 fa 00          	cmp    $0x0,%rdx
            .inc = {0x48, 0xff, 0xc2},       // 48 ff c2             	inc    %rdx
            .dec = {0x48, 0xff, 0xca},       // 48 ff ca             	dec    %rdx
            .inst_size = 2,
            .rolb = {0xc0, 0x02},            // c0 02 04             	rolb   $0x4,(%rdx)
            .rorb = {0xc0, 0x0a},            // c0 0a 04             	rorb   $0x4,(%rdx)
            .xorb = {0x80, 0x32},            // 80 32 02             	xorb   $0x2,(%rdx)
            .addb = {0x80, 0x02},            // 80 02 02             	addb   $0x2,(%rdx)
            .subb = {0x80, 0x2a},            // 80 2a 02             	subb   $0x2,(%rdx)
            .incb = {0xfe, 0x02},            // fe 02                	incb   (%rdx)
            .decb = {0xfe, 0x0a},            // fe 0a                	decb   (%rdx)
            .notb = {0xf6, 0x12},            // f6 12                	notb   (%rdx)
        },
        {
            // RBX
            .pop_size = 1,
            .pop = {0x5b},                   // 5b                   	pop    %rbx
            .cmp = {0x48, 0x83, 0xfb, 0x00}, // 48 83 fb 00          	cmp    $0x0,%rbx
            .inc = {0x48, 0xff, 0xc3},       // 48 ff c3             	inc    %rbx
            .dec = {0x48, 0xff, 0xcb},       // 48 ff cb             	dec    %rbx
            .inst_size = 2,
            .rolb = {0xc0, 0x03},            // c0 03 04             	rolb   $0x4,(%rbx)
            .rorb = {0xc0, 0x0b},            // c0 0b 04             	rorb   $0x4,(%rbx)
            .xorb = {0x80, 0x33},            // 80 33 02             	xorb   $0x2,(%rbx)
            .addb = {0x80, 0x03},            // 80 03 02             	addb   $0x2,(%rbx)
            .subb = {0x80, 0x2b},            // 80 2b 02             	subb   $0x2,(%rbx)
            .incb = {0xfe, 0x03},            // fe 03                	incb   (%rbx)
            .decb = {0xfe, 0x0b},            // fe 0b                	decb   (%rbx)
            .notb = {0xf6, 0x13},            // f6 13                	notb   (%rbx)
        },
        {
            // RSI
            .pop_size = 1,
            .pop = {0x5e},                   // 5e                   	pop    %rsi
            .cmp = {0x48, 0x83, 0xfe, 0x00}, // 48 83 fe 00          	cmp    $0x0,%rsi
            .inc = {0x48, 0xff, 0xc6},       // 48 ff c6             	inc    %rsi
            .dec = {0x48, 0xff, 0xce},       // 48 ff ce             	dec    %rsi
            .inst_size = 2,
            .rolb = {0xc0, 0x06},            // c0 06 04             	rolb   $0x4,(%rsi)
            .rorb = {0xc0, 0x0e},            // c0 0e 04             	rorb   $0x4,(%rsi)
            .xorb = {0x80, 0x36},            // 80 36 02             	xorb   $0x2,(%rsi)
            .addb = {0x80, 0x06},            // 80 06 02             	addb   $0x2,(%rsi)
            .subb = {0x80, 0x2e},            // 80 2e 02             	subb   $0x2,(%rsi)
            .incb = {0xfe, 0x06},            // fe 06                	incb   (%rsi)
            .decb = {0xfe, 0x0e},            // fe 0e                	decb   (%rsi)
            .notb = {0xf6, 0x16},            // f6 16                	notb   (%rsi)
        },
        {
            // RDI
            .pop_size = 1,
            .pop = {0x5f},                   // 5f                   	pop    %rdi
            .cmp = {0x48, 0x83, 0xff, 0x00}, // 48 83 ff 00          	cmp    $0x0,%rdi
            .inc = {0x48, 0xff, 0xc7},       // 48 ff c7             	inc    %rdi
            .dec = {0x48, 0xff, 0xcf},       // 48 ff cf             	dec    %rdi
            .inst_size = 2,
            .rolb = {0xc0, 0x07},            // c0 07 04             	rolb   $0x4,(%rdi)
            .rorb = {0xc0, 0x0f},            // c0 0f 04             	rorb   $0x4,(%rdi)
            .xorb = {0x80, 0x37},            // 80 37 02             	xorb   $0x2,(%rdi)
            .addb = {0x80, 0x07},            // 80 07 02             	addb   $0x2,(%rdi)
            .subb = {0x80, 0x2f},            // 80 2f 02             	subb   $0x2,(%rdi)
            .incb = {0xfe, 0x07},            // fe 07                	incb   (%rdi)
            .decb = {0xfe, 0x0f},            // fe 0f                	decb   (%rdi)
            .notb = {0xf6, 0x17},            // f6 17                	notb   (%rdi)
        },
        {
            // R8
            .pop_size = 2,
            .pop = {0x41, 0x58},             // 41 58                	pop    %r8
            .cmp = {0x49, 0x83, 0xf8, 0x00}, // 49 83 f8 00          	cmp    $0x0,%r8
            .inc = {0x49, 0xff, 0xc0},       // 49 ff c0             	inc    %r8
            .dec = {0x49, 0xff, 0xc8},       // 49 ff c8             	dec    %r8
            .inst_size = 3,
            .rolb = {0x41, 0xc0, 0x00},      // 41 c0 00 04          	rolb   $0x4,(%r8)
            .rorb = {0x41, 0xc0, 0x08},      // 41 c0 08 04          	rorb   $0x4,(%r8)
            .xorb = {0x41, 0x80, 0x30},      // 41 80 30 02          	xorb   $0x2,(%r8)
            .addb = {0x41, 0x80, 0x00},      // 41 80 00 02          	addb   $0x2,(%r8)
            .subb = {0x41, 0x80, 0x28},      // 41 80 28 02          	subb   $0x2,(%r8)
            .incb = {0x41, 0xfe, 0x00},      // 41 fe 00             	incb   (%r8)
            .decb = {0x41, 0xfe, 0x08},      // 41 fe 08             	decb   (%r8)
            .notb = {0x41, 0xf6, 0x10},      // 41 f6 10             	notb   (%r8)
        },
        {
            // R9
            .pop_size = 2,
            .pop = {0x41, 0x59},             // 41 59                	pop    %r9
            .cmp = {0x49, 0x83, 0xf9, 0x00}, // 49 83 f9 00          	cmp    $0x0,%r9
            .inc = {0x49, 0xff, 0xc1},       // 49 ff c1             	inc    %r9
            .dec = {0x49, 0xff, 0xc9},       // 49 ff c9             	dec    %r9
            .inst_size = 3,
            .rolb = {0x41, 0xc0, 0x01},      // 41 c0 01 04          	rolb   $0x4,(%r9)
            .rorb = {0x41, 0xc0, 0x09},      // 41 c0 09 04          	rorb   $0x4,(%r9)
            .xorb = {0x41, 0x80, 0x31},      // 41 80 31 02          	xorb   $0x2,(%r9)
            .addb = {0x41, 0x80, 0x01},      // 41 80 01 02          	addb   $0x2,(%r9)
            .subb = {0x41, 0x80, 0x29},      // 41 80 29 02          	subb   $0x2,(%r9)
            .incb = {0x41, 0xfe, 0x01},      // 41 fe 01             	incb   (%r9)
            .decb = {0x41, 0xfe, 0x09},      // 41 fe 09             	decb   (%r9)
            .notb = {0x41, 0xf6, 0x11},      // 41 f6 11             	notb   (%r9)
        },
        {
            // R10
            .pop_size = 2,
            .pop = {0x41, 0x5a},             // 41 5a                	pop    %r10
            .cmp = {0x49, 0x83, 0xfa, 0x00}, // 49 83 fa 00          	cmp    $0x0,%r10
            .inc = {0x49, 0xff, 0xc2},       // 49 ff c2             	inc    %r10
            .dec = {0x49, 0xff, 0xca},       // 49 ff ca             	dec    %r10
            .inst_size = 3,
            .rolb = {0x41, 0xc0, 0x02}, // 41 c0 02 04          	rolb   $0x4,(%r10)
            .rorb = {0x41, 0xc0, 0x0a}, // 41 c0 0a 04          	rorb   $0x4,(%r10)
            .xorb = {0x41, 0x80, 0x32},      // 41 80 32 02          	xorb   $0x2,(%r10)
            .addb = {0x41, 0x80, 0x02},      // 41 80 02 02          	addb   $0x2,(%r10)
            .subb = {0x41, 0x80, 0x2a},      // 41 80 2a 02          	subb   $0x2,(%r10)
            .incb = {0x41, 0xfe, 0x02},      // 41 fe 02             	incb   (%r10)
            .decb = {0x41, 0xfe, 0x0a},      // 41 fe 0a             	decb   (%r10)
            .notb = {0x41, 0xf6, 0x12},      // 41 f6 12             	notb   (%r10)
        },
        {
            // R11
            .pop_size = 2,
            .pop = {0x41, 0x5b},             // 41 5b                	pop    %r11
            .cmp = {0x49, 0x83, 0xfb, 0x00}, // 49 83 fb 00          	cmp    $0x0,%r11
            .inc = {0x49, 0xff, 0xc3},       // 49 ff c3             	inc    %r11
            .dec = {0x49, 0xff, 0xcb},       // 49 ff cb             	dec    %r11
            .inst_size = 3,
            .rolb = {0x41, 0xc0, 0x03},      // 41 c0 03 04          	rolb   $0x4,(%r11)
            .rorb = {0x41, 0xc0, 0x0b},      // 41 c0 0b 04          	rorb   $0x4,(%r11)
            .xorb = {0x41, 0x80, 0x33},      // 41 80 33 02          	xorb   $0x2,(%r11)
            .addb = {0x41, 0x80, 0x03},      // 41 80 03 02          	addb   $0x2,(%r11)
            .subb = {0x41, 0x80, 0x2b},      // 41 80 2b 02          	subb   $0x2,(%r11)
            .incb = {0x41, 0xfe, 0x03},      // 41 fe 03             	incb   (%r11)
            .decb = {0x41, 0xfe, 0x0b},      // 41 fe 0b             	decb   (%r11)
            .notb = {0x41, 0xf6, 0x13},      // 41 f6 13             	notb   (%r11)
        },
        {
            // R12
            .pop_size = 2,
            .pop = {0x41, 0x5c},              // 41 5c                	pop    %r12
            .cmp = {0x49, 0x83, 0xfc, 0x00},  // 49 83 fc 00          	cmp    $0x0,%r12
            .inc = {0x49, 0xff, 0xc4},        // 49 ff c4             	inc    %r12
            .dec = {0x49, 0xff, 0xcc},        // 49 ff cc             	dec    %r12
            .inst_size = 4,
            .rolb = {0x41, 0xc0, 0x04, 0x24}, // 41 c0 04 24 04       	rolb   $0x4,(%r12)
            .rorb = {0x41, 0xc0, 0x0c, 0x24}, // 41 c0 0c 24 04       	rorb   $0x4,(%r12)
            .xorb = {0x41, 0x80, 0x34, 0x24}, // 41 80 34 24 02       	xorb   $0x2,(%r12)
            .addb = {0x41, 0x80, 0x04, 0x24}, // 41 80 04 24 02       	addb   $0x2,(%r12)
            .subb = {0x41, 0x80, 0x2c, 0x24}, // 41 80 2c 24 02       	subb   $0x2,(%r12)
            .incb = {0x41, 0xfe, 0x04, 0x24}, // 41 fe 04 24          	incb   (%r12)
            .decb = {0x41, 0xfe, 0x0c, 0x24}, // 41 fe 0c 24          	decb   (%r12)
            .notb = {0x41, 0xf6, 0x14, 0x24}, // 41 f6 14 24          	notb   (%r12)
        },
        {
            // R13
            .pop_size = 2,
            .pop = {0x41, 0x5d},              // 41 5d                	pop    %r13
            .cmp = {0x49, 0x83, 0xfd, 0x00},  // 49 83 fd 00          	cmp    $0x0,%r13
            .inc = {0x49, 0xff, 0xc5},        // 49 ff c5             	inc    %r13
            .dec = {0x49, 0xff, 0xcd},        // 49 ff cd             	dec    %r13
            .inst_size = 4,
            .rolb = {0x41, 0xc0, 0x45, 0x00}, // 41 c0 45 00 04       	rolb   $0x4,0x0(%r13)
            .rorb = {0x41, 0xc0, 0x4d, 0x00}, // 41 c0 4d 00 04       	rorb   $0x4,0x0(%r13)
            .xorb = {0x41, 0x80, 0x75, 0x00}, // 41 80 75 00 02       	xorb   $0x2,0x0(%r13)
            .addb = {0x41, 0x80, 0x45, 0x00}, // 41 80 45 00 02       	addb   $0x2,0x0(%r13)
            .subb = {0x41, 0x80, 0x6d, 0x00}, // 41 80 6d 00 02       	subb   $0x2,0x0(%r13)
            .incb = {0x41, 0xfe, 0x45, 0x00}, // 41 fe 45 00          	incb   0x0(%r13)
            .decb = {0x41, 0xfe, 0x4d, 0x00}, // 41 fe 4d 00          	decb   0x0(%r13)
            .notb = {0x41, 0xf6, 0x55, 0x00}, // 41 f6 55 00          	notb   0x0(%r13)
        },
        {
            // R14
            .pop_size = 2,
            .pop = {0x41, 0x5e},              // 41 5e                	pop    %r14
            .cmp = {0x49, 0x83, 0xfe, 0x00},  // 49 83 fe 00          	cmp    $0x0,%r14
            .inc = {0x49, 0xff, 0xc6},        // 49 ff c6             	inc    %r14
            .dec = {0x49, 0xff, 0xce},        // 49 ff ce             	dec    %r14
            .inst_size = 3,
            .rolb = {0x41, 0xc0, 0x06},       // 41 c0 06 04          	rolb   $0x4,(%r14)
            .rorb = {0x41, 0xc0, 0x0e},       // 41 c0 0e 04          	rorb   $0x4,(%r14)
            .xorb = {0x41, 0x80, 0x36},       // 41 80 36 02          	xorb   $0x2,(%r14)
            .addb = {0x41, 0x80, 0x06},       // 41 80 06 02          	addb   $0x2,(%r14)
            .subb = {0x41, 0x80, 0x2e},       // 41 80 2e 02          	subb   $0x2,(%r14)
            .incb = {0x41, 0xfe, 0x06},       // 41 fe 06             	incb   (%r14)
            .decb = {0x41, 0xfe, 0x0e},       // 41 fe 0e             	decb   (%r14)
            .notb = {0x41, 0xf6, 0x16},       // 41 f6 16             	notb   (%r14)
        },
        {
            // R15
            .pop_size = 2,
            .pop = {0x41, 0x5f},              // 41 5f                	pop    %r15
            .cmp = {0x49, 0x83, 0xff, 0x00},  // 49 83 ff 00          	cmp    $0x0,%r15
            .inc = {0x49, 0xff, 0xc7},        // 49 ff c7             	inc    %r15
            .dec = {0x49, 0xff, 0xcf},        // 49 ff cf             	dec    %r15
            .inst_size = 3,
            .rolb = {0x41, 0xc0, 0x07},       // 41 c0 07 04          	rolb   $0x4,(%r15)
            .rorb = {0x41, 0xc0, 0x0f},       // 41 c0 0f 04          	rorb   $0x4,(%r15)
            .xorb = {0x41, 0x80, 0x37},       // 41 80 37 02          	xorb   $0x2,(%r15)
            .addb = {0x41, 0x80, 0x07},       // 41 80 07 02          	addb   $0x2,(%r15)
            .subb = {0x41, 0x80, 0x2f},       // 41 80 2f 02          	subb   $0x2,(%r15)
            .incb = {0x41, 0xfe, 0x07},       // 41 fe 07             	incb   (%r15)
            .decb = {0x41, 0xfe, 0x0f},       // 41 fe 0f             	decb   (%r15)
            .notb = {0x41, 0xf6, 0x17},       // 41 f6 17             	notb   (%r15)
        }
    };

    const uint8_t NOP = 0x90;
    const t_inst_data *data = &(inst_codes[inf->reg_data]);
    const t_inst_data *counter = &(inst_codes[inf->reg_counter]);

    // Write pop instructions
    const t_inst_data *tmp;
    for (uint8_t n_pop = 0; n_pop < 2; ++n_pop)
    {
        tmp = (0 == n_pop) ? counter : data;

        ft_memcpy(pop, tmp->pop, tmp->pop_size);
        pop += tmp->pop_size;

        if (2 != tmp->pop_size)
        {
            // Align with nop instructions
            *pop = NOP;
            pop++;
        }
    }

    // Write cmp command
    ft_memcpy(cmp, counter->cmp, sizeof(counter->cmp));

    // Write decryption body
    for (uint8_t inst = 0; inst < MAX_INSTRUCTION; ++inst)
    {
        uint8_t size = 0;

        if (XOR == inf->dec.instr[inst])
        {
            ft_memcpy(body, data->xorb, data->inst_size);
            *(body + data->inst_size) = inf->dec.data[inst];
            body++;
            size++;
        }
        else if (ADD == inf->dec.instr[inst])
        {
            ft_memcpy(body, data->addb, data->inst_size);
            *(body + data->inst_size) = inf->dec.data[inst];
            body++;
            size++;
        }
        else if (SUB == inf->dec.instr[inst])
        {
            ft_memcpy(body, data->subb, data->inst_size);
            *(body + data->inst_size) = inf->dec.data[inst];
            body++;
            size++;
        }
        else if (INC == inf->dec.instr[inst])
        {
            ft_memcpy(body, data->incb, data->inst_size);
        }
        else if (DEC == inf->dec.instr[inst])
        {
            ft_memcpy(body, data->decb, data->inst_size);
        }
        else if (NOT == inf->dec.instr[inst])
        {
            ft_memcpy(body, data->notb, data->inst_size);
        }
        else if (ROL == inf->dec.instr[inst])
        {
            ft_memcpy(body, data->rolb, data->inst_size);
            *(body + data->inst_size) = inf->dec.data[inst];
            body++;
            size++;
        }
        else if (ROR == inf->dec.instr[inst])
        {
            ft_memcpy(body, data->rorb, data->inst_size);
            *(body + data->inst_size) = inf->dec.data[inst];
            body++;
            size++;
        }

        size += data->inst_size;
        body += data->inst_size;

        // Align with nop instructions
        size = 5 - size;
        ft_memset(body, NOP, size);
        body += size;
    }

    ft_memcpy(body, counter->dec, sizeof(counter->dec));
    body += sizeof(counter->dec);
    ft_memcpy(body, data->inc, sizeof(data->inc));
}

void    encryption(uint8_t *enc_data, size_t enc_lenght, uint8_t *dec_start)
{
    t_enc_inf inf;

    // Initialization of required data
    init(&inf);

    uint8_t *pop;
    uint8_t *cmp;
    uint8_t *body;
    // 4 bytes for POPs operations
    pop = dec_start;
    // 4 bytes for CMP operation
    cmp = dec_start + 4;
    // 6 bytes for JMP condition - not modified
    // 100 bytes - decryption + 6 bytes inc/dec instructions
    body = dec_start + 4 +  4 + 6;

    encrypt_data(enc_data, enc_lenght, &(inf.enc));
    decryption(&inf, pop, cmp, body);
}