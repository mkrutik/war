#include "war.h"

#include <stdint.h>

static int padding(const uint8_t *data, const size_t len, t_file *res)
{
    // Padding: 1 byte = 0x80, then n bytes = 0 .... and 8 bytes for size at the end
    res->size = len + 1 + 8;
    res->size += 64 - res->size % 64;
    // Align size to memory page size
    res->mem_size = res->size + (4096 - res->size % 4096);
    res->data = (uint8_t*) ft_mmap(0, res->mem_size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if (mmap_check(res->data))
	{
		return (FAIL);
	}
    ft_memcpy(res->data, data, len);
    // Write 1 byte padding
    res->data[len] = 0x80;
    // Clean rest bytes
    ft_memset(&res->data[len + 1], 0, (res->size - len - 1));
    // Bit size of original data
    uint64_t bite_len = len * 8;
    *((uint64_t*)&res->data[res->size - 8]) = bite_len;
    return SUCCESS;
}

static void  blocks_to_hex(uint32_t blocks[4], char arr[32])
{
    uint8_t     tmp;
    uint8_t     *data;
    int         index;
    uint8_t     byte;

    data = (uint8_t*) blocks;
    index = 0;
    byte = 0;

    while (byte < 16)
    {
        tmp = data[byte];

        arr[index + 1] = (tmp % 16) + ((tmp % 16 > 9) ? ('a' - 10) : '0');
        tmp /= 16;
        arr[index] = (tmp % 16) + ((tmp % 16 > 9) ? ('a' - 10) : '0');

        byte++;
        index += 2;
    }
}

static void md5_operate_block(uint32_t *a, uint32_t *b, uint32_t *c, uint32_t *d, uint32_t *msg)
{
    const uint8_t shift[64] = {
        7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
        5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20,
        4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
        6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21
    };

    const uint32_t md5_k_first[32] = {
        0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, 0xf57c0faf, 0x4787c62a,
        0xa8304613, 0xfd469501, 0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
        0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
        0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 0xd62f105d, 0x02441453,
        0xd8a1e681, 0xe7d3fbc8, 0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
        0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
    };

    const uint32_t md5_k_second[32] = {
        0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c, 0xa4beea44, 0x4bdecfa9,
        0xf6bb4b60, 0xbebfbc70, 0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
        0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
        0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039, 0x655b59c3, 0x8f0ccc92,
        0xffeff47d, 0x85845dd1, 0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
        0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
    };

    const uint8_t msg_index[64] = {
        0, 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
        1, 6, 11,  0,  5, 10, 15,  4,  9, 14,  3,  8, 13,  2,  7, 12,
        5, 8, 11, 14,  1,  4,  7, 10, 13,  0,  3,  6,  9, 12, 15,  2,
        0, 7, 14,  5, 12,  3, 10,  1,  8, 15,  6, 13,  4, 11,  2,  9,
    };

    uint8_t     i = 0;;
    uint32_t    f = 0;
    uint32_t    tmp;

    while (i < 64)
    {
        if (i < 16)
            f = ((*b & *c) | ((~*b) & *d));
        else if (i < 32)
            f = ((*b & *d) | (*c & (~*d)));
        else if (i < 48)
            f = (*b ^ *c ^ *d);
        else
            f = (*c ^ (*b | (~*d)));

        tmp = *a + f + msg[msg_index[i]] + ((i < 32) ? md5_k_first[i] : md5_k_second[i - 31]); //  + md5_k[i];
        *a = *b + ((tmp << shift[i]) | (tmp >> (32 - shift[i])));

        // rotation
        tmp = *a;
        *a = *d;
        *d = *c;
        *c = *b;
        *b = tmp;

        i++;
    }
}

void   md5(const uint8_t *msg, const size_t len, char hash[32])
{
    const uint32_t md5_init[4] = {
        0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476,
    };

    uint32_t    tmp[4];
    uint32_t    r[4];
    size_t      i;
    t_file      padded;

    if (padding(msg, len, &padded) == -1)
        return ;

    ft_memcpy(r, md5_init, 16);
    i = 0;
    while (i < padded.size)
    {
        ft_memcpy(tmp, r, 16);
        md5_operate_block(&tmp[0], &tmp[1], &tmp[2], &tmp[3], (uint32_t*)(padded.data + i));
        // Add to result
        r[0] += tmp[0];
        r[1] += tmp[1];
        r[2] += tmp[2];
        r[3] += tmp[3];
        i += 64;
    }
    ft_munmap(padded.data, padded.mem_size);
    blocks_to_hex(r, hash);
}