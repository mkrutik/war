/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xor_encrypt.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 15:02:26 by adzikovs          #+#    #+#             */
/*   Updated: 2019/07/21 15:02:26 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <stddef.h>

void	xor_encrypt(unsigned char *data, size_t len, unsigned char key)
{
	while (len)
	{
		*data = *data ^ key;
		data++;
		len--;
	}
}
